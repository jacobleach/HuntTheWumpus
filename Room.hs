module Room
( Room
, mkRoom
, addConnection
, roomID
, content
, connections
) where

data Room = Room { roomID :: Int, content :: Int, connections :: [Int]}
    deriving (Show)

mkRoom :: Int -> Int -> [Int] -> Room
mkRoom i c r = Room i c r 

addConnection :: Room -> Int -> Room
addConnection r i = mkRoom (roomID r) (content r) ((connections r) ++ ([i]))
