import Map
import Player
import System.Random

main = do
    putStrLn "Welcome to Hunt the Wumpus"
    rnd1 <- randomRIO (1,20)
    rnd2 <- randomRIO (1,20)
    gameloop (generateMap 20 3 rnd1 rnd2) 

gameloop m = do
    putStrLn ("You are in room: " ++ (show (location (_player m))))
    putStrLn ("The rooms around you are: " ++ (show (getAdjRooms m)))
    putStrLn (smell m) 
    putStrLn "Would you like to move (m) or throw a holy hand grenade (s)?"
    ans <- getLine
    putStrLn "Enter a room number: "
    room <- getLine
    if ans == "m" then
        check (move m (read room))    
    else if ans == "s" then
        check (shoot m (read room))
    else
        gameloop m 
check m = 
    if (isWin m) then 
        putStrLn "You killed the Wumpus with a holy hand grenade! You win! Great job! Amen!"
    else if (isLose m) then 
        putStrLn "You: Tis but a scratch.\nKing Wumpus: A scratch? Your arm's off.\nYou: No it isn't.\nKing Wumpus: What's that, then\nYou: I've had worse.\nKing Wumpus: You liar.\nYou: Come on ya pansy.\nKing Wumpus just ripped your last leg off!\nYou: All right, we'll call it a draw.\nGameover."
    else 
        gameloop m

