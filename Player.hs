module Player
( Player
, player
, ammo
, location
) where

data Player = Player { ammo :: Int, location :: Int }

player :: Int -> Int -> Player
player x y = Player x y


