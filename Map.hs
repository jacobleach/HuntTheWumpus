module Map
( Map
, generateMap
, _player
, getAdjRooms
, contains
, shoot
, move
, isLose
, isWin
, smell
) where

import Room 
import Player

data Map = Map { rooms :: [Room],  _player :: Player, wumpus :: Int }

instance Show Map where 
    show m = "Room Connections\n" ++ showRooms (rooms m)
        where   showRooms [] = []
                showRooms r  = "Room ID " ++ show (roomID (head r)) ++ ": " ++ (show (connections (head r))) ++ "\n" ++ (showRooms (tail r))
 
 
--The map it generates is also boring and always the same
--But it works!
--Placement is not yet random. Sadly.
generateMap :: Int -> Int -> Int -> Int -> Map
generateMap num arr pla wum = Map (linkRooms2 (mkCircle (mkRoomArray num))) (player arr pla) wum

getAdjRooms :: Map -> [Int]
getAdjRooms m = connections (getRoom m (location (_player m)))  

getRoom :: Map -> Int -> Room
getRoom m n = (rooms m) !! (n - 1)

contains :: Map -> Int -> Bool
contains m l
    | (wumpus m) == l
        = True
    | otherwise
        = False

shoot :: Map -> Int -> Map
shoot m l
    | contains m l
        = Map (rooms m) (player ((ammo (_player m)) -1) (location (_player m))) (-1)
    | otherwise
        = Map (rooms m) (player ((ammo (_player m)) - 1) (location (_player m))) (wumpus m)

move :: Map -> Int -> Map
move m l 
    | contains m l
        = Map (rooms m) (player (-1) l) (wumpus m)  
    | otherwise 
        = Map (rooms m) (player (ammo (_player m)) l) (wumpus m)

smell :: Map -> String
smell m 
    | (length (filter (== (wumpus m)) (getAdjRooms m))) > 0 
        = "Something smells around here"
    | otherwise
        = "There ain't no smell'"

isLose m
    | (ammo (_player m)) > 0
        = False
    | otherwise
        = True

isWin m
    | (wumpus m) < 0  
        = True
    | otherwise     
        = False

--Links rooms that are 1 away
linkRooms2 :: [Room] -> [Room]
linkRooms2 xs = helper2 (helper xs) 
    where  helper [] = []
           helper xs1
            | (((roomID (head xs1)) + 2) `mod` (length xs)) == 0
                = [addConnection (head xs1) ((roomID (head xs1)) + 2)] ++ helper (tail xs1)
            | otherwise 
                = [addConnection (head xs1) (((roomID (head xs1)) + 2) `mod` (length xs))] ++ helper (tail xs1)
           
           helper2 []  = []
           helper2 xs1 
            | (((roomID (head xs1)) - 2) `mod` (length xs)) == 0 
                = [addConnection (head xs1) (length xs)] ++ helper2 (tail xs1)
            | otherwise 
                = [addConnection (head xs1) (((roomID (head xs1)) - 2) `mod` (length xs))] ++ helper2 (tail xs1)

mkCircle :: [Room] -> [Room]
mkCircle [] = []
mkCircle xs = reverse (worker (reverse (worker xs xs)) (reverse xs))
    where worker ys zs
            | length ys == 1   = [addConn (roomID (head zs))]
            | otherwise        = [addConn getNextID] ++ (worker (tail ys) zs)
            where getNextID    = (roomID (head (tail ys)))
                  addConn id   = addConnection (head ys) id

mkRoomArray :: Int -> [Room]
mkRoomArray 0 = []
mkRoomArray x = mkRoomArray (x - 1)  ++ [mkRoom x 0 []]

{-- Deprecated

linkRooms :: [Room] -> [Room]
linkRooms xs
    | roomID (head xs) == 1 = [addConnection (addConnection (head xs) (roomID (last xs))) (roomID (head (tail xs)))] ++ linkRooms (([addConnection (head (tail xs)) 1]) ++  (tail (tail xs)))
    | length xs <= 1        = [addConnection (head xs) 1]
    | otherwise             = [addConnection (head xs) (roomID (head (tail xs)))] ++ (linkRooms ([addConnection (head (tail xs)) (roomID (head xs))] ++ (tail (tail xs))))

--}

